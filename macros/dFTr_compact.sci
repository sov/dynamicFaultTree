function [y] = dFTr_compact(x)
// compacts a  multiset distribution tlist
// Calling Sequence
//   Y=dFTree_compact(X)
// Parameters
//   X : <link  linkend="attackTree_distribution_tlist">a sorted multiset distribution tlist.</link>
//   Y : <link  linkend="attackTree_distribution_tlist">a sorted  distribution tlist.</link>
//
// Description
//   returns Y the compacted discrete  distribution tlist obtained from X. 
//   The states of X must be sorted in ascending order.
//   In X, a state s can appear several times with different
//   probabilities. In Y, each state s appears only once with the sum of 
//   its probabilities. 
// Examples
// X=tlist(['distribution','state','probability'],[2,2,9,15],[0.125,0.125,0.5,0.25])
// Y=dFTr_compact(X)



// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         x: tlist distribution

// Returns:
//       tlist distribution compactDistribution(x) 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)
y=tlist(['distribution','state','probability'],[],[]);
k=1;
for i=2:length(x.state)
  if x.state(i)==x.state(k) then
    x.probability(k)=x.probability(k)+x.probability(i);
  else
    k=k+1;
    x.state(k)=x.state(i);
    x.probability(k)=x.probability(i);
  end;
end;

//print(%io(2),xy.probability);

y.state=resize_matrix(x.state,1,k);
y.probability=resize_matrix(x.probability,1,k);

//print(%io(2),xy.probability);

endfunction
