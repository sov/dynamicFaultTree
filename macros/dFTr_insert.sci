function [xy,k] =dFTr_insert(state,probability,xy,k)

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         state: 
//         probability: 
//         xy
//         k

// Returns:
//        [xy,k] =dFTr_insert(state,probability,xy,k)
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
done=%F;
for i=1:k
  if xy.state(i)==state then
    xy.probability(i)=xy.probability(i)+probability;
    done=%T;
    break;
  elseif xy.state(i) > state then
    for j=k:-1:i
      xy.state(j+1)=xy.state(j);
      xy.probability(j+1)=xy.probability(j);
    end;
    k=k+1;
    xy.state(i)=state;
    xy.probability(i)=probability;
    done=%T;
    break;
  end;
end;
if done==%F then
  k=k+1;
  xy.state(k)=state;
  xy.probability(k)=probability;
  done=%T;
end;
//print(%io(2),k);

endfunction



