function [xy] = dFTr_2of3(v1,v2,v3)
// sorted distribution tlist 2 out of 3 (2/3)
// Calling Sequence
//   Z=dFTr_2of3(v1,v2,v3)
// Parameters
//   v1 : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   v2 : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   v3 : <link  linkend="dFTr_distribution_tlist">a sorted tlist distribution tlist.</link>
//
// Description
//   Returns Z the sorted distribution tlist 2 out of 3 of v1,v2 and v3 defined by :
// <para>
//<latex> \LARGE
//$ min \{ v_i | \sum_{j=1}^I {\mathbb{1}_{(v_j<v_i)} } \geq k \} $ 
//    </latex>
// </para>
// Examples
// v1=tlist(['distribution','state','probability'],[2,4,10],[0.2,0.4,0.4])
// v2=tlist(['distribution','state','probability'],[1,3,4,10],[0.1,0.3,0.2,0.4])
// v3=tlist(['distribution','state','probability'],[2,3,5,10],[0.2,0.3,0.2,0.5])

// Z=dFTr_2of3(v1,v2,v3)


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         v1: tlist distribution
//         v2: tlist distribution
//         v3: tlist distribution
// Returns:
//       tlist distribution dFTr_2of3(v1,v2,v3) 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)

i=1;
j=1;
k=1;
xyLength=length(v1.state)+length(v2.state)+length(v3.state);
xy=tlist(['distribution','state','probability'],zeros(1,xyLength),zeros(1,xyLength));

pcum=1;
qcum=1;
rcum=1;
while (i<length(v1.state)+1) &  (j <length(v2.state)+1) & (k <length(v3.state)+1)
 	

  p1=v1.probability(i);
  d1=v1.state(i);
  
  p2=v2.probability(j);
  d2=v2.state(j);

  p3=v3.probability(k);
  d3=v3.state(k);
  //Testing order of d1 d2 d3
  status=dFTr_status(d1,d2,d3);

  select status
	case 0 then // equal
		val=p2*(1-pcum)*rcum +p2*pcum*(1-rcum)+p3*(1-pcum)*qcum +p3*pcum*(1-qcum)+p1*(1-rcum)*qcum +p1*rcum*(1-qcum)+p1*p2*(rcum-p3)+p1*p3*(qcum-p2)+ +p3*p2*(pcum-p1)+        -p3*p2*(1-pcum) -p1*p2*(1-rcum) -p3*p1*(1-qcum)+p1*p2*p3;
		xy.state(d1) = d1;
		xy.probability(d1) = val;
		pcum = pcum - p1;
		qcum = qcum - p2;
		rcum = rcum - p3;
		i=i+1;
		j=j+1;
		k=k+1;
	case 1 then // d1=d2 d3>d2	|| d1=d3 d2 >d3 || 	d2=d3 d1>d3 || 
		
		dRet=dFTr_oneDiff(d1,d2,d3);
		xy.state(dRet(3)) = dRet(3);
		xy.probability(dRet(3)) = dRet(1);

		if dRet(2) == 1 then
			pcum = pcum - p1;
			qcum = qcum - p2;
			i=i+1;
			j=j+1;
		else 
			if dRet(2) == 2 then
				pcum = pcum - p1;
				rcum = rcum - p3;
				i=i+1;
				k=k+1;
			else
				if dRet(2) == 3 then
					qcum = qcum - p2;
					rcum = rcum - p3;
					j=j+1;
					k=k+1;
				else
					print(%io(2),"ERROR");
				end;
			end;
		end;
    case 2 then  // d1< d2 && d1<d3 ||  d2<d1 && d2<d3 ||  d3<d1 && d3<d2
    	dRet=dFTr_oneMin(d1,d2,d3);
		xy.state(dRet(3))= dRet(3);
		xy.probability(dRet(3))= dRet(1);
		if dRet(2) == 1 then 
			pcum = pcum - p1;
			i=i+1;
		else
			if dRet(2) == 2 then 
				qcum = qcum - p2;
				j=j+1;
			else
				if dRet(2) == 3 then 
					rcum = rcum - p3;
					k=k+1;
				else
					print(%io(2),"ERROR");
				end;
			end;
		end;
	end;
end;

xy=dFTr_sortState(xy);
xy=dFTr_compact(xy);

endfunction
