function [lowerBound] = dFTr_lowerBound(dist,lowerBoundSize)
// lowerBound with a limited number of states
// Calling Sequence
// lowerBound=dFTr_lowerBound(dist,lowerBoundSize)
// Parameters
//   dist : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//   lowerBoundSize : number of states wanted in the lower bound distribution
//
// Description 
//    returns <varname>lowerBound</varname>, a lower bound sorted distribution tlist of <varname>dist</varname> with <varname>lowerBoundSize</varname> states.
// Examples
// [d1_inf50,d1_sup50]=dFTr_erlangBounds(2,1,12.5,4)
// d1_lower20=dFTr_lowerBound(d1_inf50,20)
// clf()
// plot2d2(d1_inf50.state,cumsum(d1_inf50.probability),color('red'));
// plot2d2(d1_lower20.state,cumsum(d1_lower20.probability),color('green'));
// legends(['original';'lower bound'],[color('red') color('green')],opt="lr")

// Number of arguments in function call
[%nargout,%nargin] = argn(0)
// Display mode
mode(0);
// Display warning for floating point exception
ieee(1);
if %nargin<2 then
  error("dFTr_lowerBounds needs two arguments");
end;
n = max(size(dist.state));
// Output variables initialisation (not found in input variables)
lowerBound=tlist(['distribution','state','probability'],zeros(1,lowerBoundSize),zeros(1,lowerBoundSize));

bulkSize=floor(n/lowerBoundSize);
remain=modulo(n,lowerBoundSize);

// print(%io(2),bulkSize,remain);
p=1;
for i = 1:bulkSize:n-remain
//  print(%io(2),i);
  lowerBound.state(p)=dist.state(i);
  for j = i:i+bulkSize-1
//     print(%io(2),p,j,D_l);
    lowerBound.probability(p)=lowerBound.probability(p)+dist.probability(j);
  end;
  p=p+1;
end;
for i=1:remain
 lowerBound.probability(lowerBoundSize)=lowerBound.probability(lowerBoundSize)+dist.probability(n-remain+i);
end

endfunction
