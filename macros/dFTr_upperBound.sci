function [upperBound] = dFTr_upperBound(dist,upperBoundSize)
// upperBound with a limited number of states
// Calling Sequence
// upperBound=dFTr_upperBound(dist,upperBoundSize)
// Parameters
//   dist : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//   upperBoundSize : number of states wanted in the upper bound distribution
//
// Description 
//    returns <varname>upperBound</varname>, an upper bound sorted distribution tlist of <varname>dist</varname> with <varname>upperBoundSize</varname> states.
// Examples
// [d1_inf50,d1_sup50]=dFTr_erlangBounds(2,1,12.5,4)
// d1_upper20=dFTr_upperBound(d1_sup50,20)
// clf()
// plot2d2(d1_sup50.state,cumsum(d1_sup50.probability),color('red'));
// plot2d2(d1_upper20.state,cumsum(d1_upper20.probability),color('green'));
// legends(['original';'upper bound'],[color('red') color('green')],opt="lr")

// Output variables initialisation (not found in input variables)

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);

if %nargin<2 then
  error("dFTr_upperBound needs two arguments");
end;
n = max(size(dist.state));
// Output variables initialisation (not found in input variables)
upperBound=tlist(['distribution','state','probability'],zeros(1,upperBoundSize),zeros(1,upperBoundSize));

bulkSize=floor(n/upperBoundSize);
remain=modulo(n,upperBoundSize);

// print(%io(2),bulkSize,remain);
p=1;
for i = 1:bulkSize:n-remain
//  print(%io(2),i);
  upperBound.state(p)=dist.state(i+bulkSize-1);
  for j = i:i+bulkSize-1
//     print(%io(2),p,j,D_l);
    upperBound.probability(p)=upperBound.probability(p)+dist.probability(j);
  end
  p=p+1;
end
upperBound.state(upperBoundSize)=dist.state(n);
for i=1:remain
 upperBound.probability(upperBoundSize)=upperBound.probability(upperBoundSize)+dist.probability(n-remain+i);
end

endfunction
