function [xy] = dFTr_or(x,y)
// sorted distribution tlist disjunction (or)
// Calling Sequence
//   Z=dFTr_or(X,Y)
// Parameters
//   X : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//   Y : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//   Z : <link  linkend="attackTree_distribution_tlist">a sorted tlist distribution tlist</link>
//
// Description
//   Returns <varname>Z</varname> the sorted distribution tlist disjunction of <varname>X</varname> and <varname>Y</varname> defined by :
// <para>
//    <latex>
//     \Pr (Z =a)=  \Pr(X =a) \times \Pr(Y >a)+ \Pr(Y =a) \times \Pr(X>a)+ \Pr(X =a) \times \Pr(Y=a).
//    </latex>
//    </para>
// Examples
// X=tlist(['distribution','state','probability'],[2,9,15],[0.25,0.5,0.25])
// Y=tlist(['distribution','state','probability'],[5,7],[0.3,0.7])
// Z=dFTr_or(X,Y)
// // Expected : Z.state         2.   5.    7.
// //            Z.probability   0.25 0.225 0.525


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         x: tlist distribution
//         y: tlist distribution

// Returns:
//       tlist distribution dFTr_or(x,y) 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)
xyLength=length(x.state)+length(y.state);
xy=tlist(['distribution','state','probability'],zeros(1,xyLength),zeros(1,xyLength));
k=0;
p=1;
q=1;
XCum=1;
YCum=1;
while (p<=length(x.state)) & (q <= length(y.state))
  k=k+1;
  if x.state(p) < y.state(q) then
    xy.state(k)=x.state(p);
    xy.probability(k)=x.probability(p)*YCum;
    XCum=XCum-x.probability(p);
    if XCum<0 then
       XCum=0;
    end;
    p=p+1;
    if xy.probability(k)<0 then
     error('negative probability');
    end;
  elseif x.state(p) > y.state(q) then
    xy.state(k)=y.state(q);
    xy.probability(k)=y.probability(q)*XCum;
    YCum=YCum-y.probability(q);
    if YCum < 0 then
       YCum=0;
    end;
    q=q+1;
    if xy.probability(k)<0 then
      error('negative probability');
    end;
  else
    xy.state(k)=x.state(p);
    xy.probability(k)=x.probability(p)*YCum+y.probability(q)*XCum-x.probability(p)*y.probability(q);
    if xy.probability(k)<0 then
//      print(%io(2),XCum,YCum,x.probability(p),y.probability(q),k,p,q);
      error('negative probability');
    end;
    XCum=XCum-x.probability(p);
   if XCum<0 then
      XCum=0;
    end;
    YCum=YCum-y.probability(q);
   if YCum < 0 then
       YCum=0;
    end;
    p=p+1;
    q=q+1;
    
  end;
  
end;    


xy.state=resize_matrix(xy.state,1,k);
xy.probability=resize_matrix(xy.probability,1,k);
//print(%io(2),xy.probability);
cumXY=sum(xy.probability);
if cumXY<1 then
  warning('dFTr_or : cum sum <1 ');
end;


endfunction
