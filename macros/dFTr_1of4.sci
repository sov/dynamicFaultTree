function [xy] = dFTr_1of4(v1,v2,v3,v4)
// sorted distribution tlist 1 out of 4 (1/4)
// Calling Sequence
//   Z=dFTr_1of4(v1,v2,v3,v4)
// Parameters
//   v1 : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   v2 : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   v3 : <link  linkend="dFTr_distribution_tlist">a sorted tlist distribution tlist.</link>
//	 v4 : <link  linkend="dFTr_distribution_tlist">a sorted tlist distribution tlist.</link>
//
// Description
//   Returns Z the sorted distribution tlist 1 out of 4 of v1,v2,v3 and v4 defined by :
// <para>
//<latex> \LARGE
//$ min \{ v_i | \sum_{j=1}^I {\mathbb{1}_{(v_j<v_i)} } \geq k \} $ 
//    </latex>
// </para>
// Examples
// v1=tlist(['distribution','state','probability'),[2,4,10),[0.2,0.4,0.4))
// v2=tlist(['distribution','state','probability'),[1,3,4,10),[0.1,0.3,0.2,0.4))
// v3=tlist(['distribution','state','probability'),[2,3,5,10),[0.2,0.2,0.1,0.5))
// v4=tlist(['distribution','state','probability'),[2,4,10),[0.2,0.4,0.4))

// Z=dFTr_1of4(v1,v2,v3,v4)


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         v1: tlist distribution
//         v2: tlist distribution
//         v3: tlist distribution
//         v4: tlist distribution
// Returns:
//       tlist distribution dFTr_1of4(v1,v2,v3,v4) 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)

xyLength=length(v1.state)+length(v2.state)+length(v3.state)+length(v4.state);
xy=tlist(['distribution','state','probability'],zeros(1,xyLength),zeros(1,xyLength));

xy=dFTr_or(v1,v2);
xy=dFTr_or(xy,v3);
xy=dFTr_or(xy,v4);

endfunction
