function [xy] = dFTr_2of4(v1,v2,v3,v4)
// sorted distribution tlist 2 out of 4 (2/4)
// Calling Sequence
//   Z=dFTr_2of4(v1,v2,v3,v4)
// Parameters
//   v1 : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   v2 : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   v3 : <link  linkend="dFTr_distribution_tlist">a sorted tlist distribution tlist.</link>
//	 v4 : <link  linkend="dFTr_distribution_tlist">a sorted tlist distribution tlist.</link>
//
// Description
//   Returns Z the sorted distribution tlist 2 out of 4 of v1,v2,v3 and v4 defined by :
// <para>
//<latex> \LARGE
//$ min \{ v_i | \sum_{j=1}^I {\mathbb{1}_{(v_j<v_i)} } \geq k \} $ 
//    </latex>
// </para>
// Examples
// v1=tlist(['distribution','state','probability'),[2,4,10],[0.2,0.4,0.4])
// v2=tlist(['distribution','state','probability'),[1,3,4,10],[0.1,0.3,0.2,0.4])
// v3=tlist(['distribution','state','probability'),[2,3,5,10],[0.2,0.2,0.1,0.5])
// v4=tlist(['distribution','state','probability'),[1,2,7,8],[0.3,0.2,0.2,0.3])

// Z=dFTr_2of4(v1,v2,v3,v4)


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         v1: tlist distribution
//         v2: tlist distribution
//         v3: tlist distribution
//         v4: tlist distribution
// Returns:
//       tlist distribution dFTr_2of4(v1,v2,v3,v4) 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)

i=1;
j=1;
k=1;
l=1;
xyLength=length(v1.state)+length(v2.state)+length(v3.state)+length(v4.state);
xy=tlist(['distribution','state','probability'],zeros(1,xyLength),zeros(1,xyLength));

pcum=1.0;
qcum=1.0;
rcum=1.0;
wcum=1.0;
ptab=list();
dtab=list();
cum=list();

while (i<length(v1.state)+1) &  (j <length(v2.state)+1) & (k <length(v3.state)+1) & (l <length(v4.state)+1)
 	
  p1=v1.probability(i);
  d1=v1.state(i);
  
  p2=v2.probability(j);
  d2=v2.state(j);

  p3=v3.probability(k);
  d3=v3.state(k);

  p4=v4.probability(l);
  d4=v4.state(l);
  
	if (d1 <d2) & (d1 <d3) & (d1 <d4) then
	 	egal=1; mini=1;
		ptab(1)=p1; dtab(1)=d1; cum(1)=pcum;
		ptab(2)=p2; dtab(2)=d2; cum(2)=qcum;
		ptab(3)=p3; dtab(3)=d3; cum(3)=rcum;
		ptab(4)=p4; dtab(4)=d4; cum(4)=wcum;
	end;
	if (d2 <d1) & (d2 <d3) &(d2 <d4) then
	 	egal=1;mini=2;
		ptab(1)=p2; dtab(1)=d2; cum(1)=qcum;
		ptab(2)=p1; dtab(2)=d1;  cum(2)=pcum;
		ptab(3)=p3; dtab(3)=d3;  cum(3)=rcum;
		ptab(4)=p4; dtab(4)=d4;  cum(4)=wcum;
	end;
	if (d3 <d1) & (d3 <d2) & (d3 <d4) then
	 	egal=1; mini=3;
		ptab(1)=p3; dtab(1)=d3;  cum(1)=rcum;
		ptab(2)=p1; dtab(2)=d1;  cum(2)=pcum;
		ptab(3)=p2; dtab(3)=d2;  cum(3)=qcum;
		ptab(4)=p4; dtab(4)=d4;  cum(4)=wcum;
	end;
	if (d4 <d1) & (d4 <d2) & (d4 <d3) then
	 	egal=1; mini=4;
		ptab(1)=p4; dtab(1)=d4;  cum(1)=wcum;
		ptab(2)=p1; dtab(2)=d1;  cum(2)=pcum;
		ptab(3)=p2; dtab(3)=d2;  cum(3)=qcum;
		ptab(4)=p3; dtab(4)=d3;  cum(4)=rcum;
	end;
	// 2 egaux
	if (d1 ==d2) & (d1 <d3) & (d1 <d4) then
	 	egal=2;mini=1; //1 2
		ptab(1)=p1; dtab(1)=d1; cum(1)=pcum;
		ptab(2)=p2; dtab(2)=d2; cum(2)=qcum;
		ptab(3)=p3; dtab(3)=d3; cum(3)=rcum;
		ptab(4)=p4; dtab(4)=d4; cum(4)=wcum;
	end;
	if (d1 ==d3) & (d1 <d2) & (d1 <d4) then
	 	egal=2;mini=2; // 1 3
		ptab(1)=p1; dtab(1)=d1;  cum(1)=pcum;
		ptab(2)=p3; dtab(2)=d3;  cum(2)=rcum;
		ptab(3)=p2; dtab(3)=d2;  cum(3)=qcum;
		ptab(4)=p4; dtab(4)=d4;  cum(4)=wcum;
	end;
	if (d4 ==d1) & (d4 <d2) & (d4 <d3) then
	 	egal=2; mini=3; // 1 4
		ptab(1)=p1; dtab(1)=d1;  cum(1)=pcum;
		ptab(2)=p4; dtab(2)=d4;  cum(2)=wcum;
		ptab(3)=p2; dtab(3)=d2;  cum(3)=qcum;
		ptab(4)=p3; dtab(4)=d3;  cum(4)=rcum;
	end;
	if (d2 ==d3) & (d2 <d1) & (d2 <d4) then
	 	egal=2; mini=4; // 2 3 
		ptab(1)=p2; dtab(1)=d2;  cum(1)=qcum;
		ptab(2)=p3; dtab(2)=d3;  cum(2)=rcum;
		ptab(3)=p1; dtab(3)=d1;  cum(3)=pcum;
		ptab(4)=p4; dtab(4)=d4;  cum(4)=wcum;
	end;
	if (d2 ==d4) & (d2 <d1) & (d2 <d3) then
	 	egal=2; mini=5; // 2 4
		ptab(1)=p2; dtab(1)=d2;  cum(1)=qcum;
		ptab(2)=p4; dtab(2)=d4;  cum(2)=wcum;
		ptab(3)=p1; dtab(3)=d1;  cum(3)=pcum;
		ptab(4)=p3; dtab(4)=d3;  cum(4)=rcum;
	end;
	if (d3 ==d4) & (d3 <d2) & (d3 <d1) then
	 	egal=2; mini=6; // 3 4
		ptab(1)=p3; dtab(1)=d3;  cum(1)=rcum;
		ptab(2)=p4; dtab(2)=d4;  cum(2)=wcum;
		ptab(3)=p2; dtab(3)=d2;  cum(3)=qcum;
		ptab(4)=p1; dtab(4)=d1;  cum(4)=pcum;
	end;
	//3 egaux	
	if (d1 ==d2) & (d3 ==d2) & (d1 <d4) then
	 	egal=3; mini=1; // 1 2 3
		ptab(1)=p1; dtab(1)=d1; cum(1)=pcum;
		ptab(2)=p2; dtab(2)=d2;  cum(2)=qcum;
		ptab(3)=p3; dtab(3)=d3;  cum(3)=rcum;
		ptab(4)=p4; dtab(4)=d4;  cum(4)=wcum;
	end;
	
	if (d3 ==d4) & (d3 ==d2) & (d3 <d1) then
	 	egal=3; mini=2; // 2 3 4
		ptab(1)=p2; dtab(1)=d2;  cum(1)=qcum;
		ptab(2)=p3; dtab(2)=d3;  cum(2)=rcum;
		ptab(3)=p4; dtab(3)=d4;  cum(3)=wcum;
		ptab(4)=p1; dtab(4)=d1;  cum(4)=pcum;
	end;
	
	if (d1 ==d2) & (d2 ==d4) & (d1 <d3) then
	 	egal=3; mini=3; //  1 2  4
		ptab(1)=p1; dtab(1)=d1;  cum(1)=pcum;
		ptab(2)=p2; dtab(2)=d2;  cum(2)=qcum;
		ptab(3)=p4; dtab(3)=d4;  cum(3)=wcum;
		ptab(4)=p3; dtab(4)=d3;  cum(4)=rcum;
	end;
	
	if (d3 ==d4) & (d3 ==d1) & (d3 <d2) then
	 	egal=3; mini=4; // 1 3 4
		ptab(1)=p1; dtab(1)=d1;  cum(1)=pcum;
		ptab(2)=p3; dtab(2)=d3;  cum(2)=rcum;
		ptab(3)=p4; dtab(3)=d4;  cum(3)=wcum;
		ptab(4)=p2; dtab(4)=d2;  cum(4)=qcum;
	end;
	
	if (d3 ==d4) & (d3 ==d1) & (d3 ==d2) then
	 	egal=4; mini=0; // 1 2  3 4
		ptab(1)=p1; dtab(1)=d1;  cum(1)=pcum;
		ptab(2)=p2; dtab(2)=d2;  cum(2)=qcum;
		ptab(3)=p3; dtab(3)=d3;  cum(3)=rcum;
		ptab(4)=p4; dtab(4)=d4;  cum(4)=wcum;
	end;

  select egal
	case 1 then // 2 out of 4 when only 1 min
		val= ptab(1)*(1- cum(2))*cum(3)*cum(4) + ptab(1)*(1-cum(3))*cum(2)*cum(4) + ptab(1)*(1-cum(4))*cum(2)*cum(3);
		if (val> 0) then
			dx=dtab(1);
			xy.state(dx)=dx;
			xy.probability(dx)=val;
		end;

		if (mini==1) then pcum = pcum - p1; i = i+1 ; end;
		if (mini==2) then qcum = qcum - p2; j = j+1 ; end;
		if (mini==3) then rcum = rcum - p3; k = k+1 ; end;
		if (mini==4) then wcum = wcum - p4; l = l+1 ; end;
    case 2 then   // 2 out of K when there are 2 min
		val= ptab(1)*ptab(2)* ((1-cum(3)) *cum(4) + (1-cum(4)) *cum(3)  + cum(3)*cum(4) ) + ( ptab(1)*(cum(2)-ptab(2)) + ptab(2)*(cum(1)-ptab(1))) * ((1-cum(3)) *cum(4) + (1-cum(4))*cum(3))+ ( ptab(1)*(1- cum(2)) + ptab(2)*( 1-cum(1))) * cum(3)*cum(4); // 2 faulty +1 min
		if (val> 0) then
			dx=dtab(1);
			xy.state(dx)=dx;
			xy.probability(dx)=val;
		end;

		if (mini==1) then pcum = pcum - p1; qcum = qcum - p2; i = i+1 ;  j = j+1 ;  end; 
		if (mini==2) then pcum = pcum - p1; rcum = rcum - p3; i = i+1 ;  k = k+1 ;  end;
		if (mini==3) then pcum = pcum - p1; wcum = wcum - p4; i = i+1 ;  l = l+1 ;  end;
		if (mini==4) then qcum = qcum - p2; rcum = rcum - p3; j = j+1 ;  k = k+1 ;  end; 
		if (mini==5) then qcum = qcum - p2; wcum = wcum - p4; j = j+1 ;  l = l+1 ;  end;
		if (mini==6) then rcum = rcum - p3; wcum = wcum - p4; k = k+1 ;  l = l+1 ;  end;
    
    case 3 then //2 outof 4 when 3 min	
										
		// conditionning on  the smallest 3 values 
		val=(ptab(1)*(cum(2)-ptab(2))*(cum(3)-ptab(3)) + ptab(2)* (cum(1)-ptab(1))*(cum(3)-ptab(3)) +ptab(3)*(cum(2)-ptab(2))*(cum(1)-ptab(1)))    * (1-cum(4)) + (ptab(1)*(cum(2)-ptab(2))*(1-cum(3)) + ptab(1)*(cum(3)-ptab(3))*(1-cum(2)) + ptab(2)*(cum(1)-ptab(1))*(1-cum(3)) + ptab(2)*(cum(3)-ptab(3))*(1-cum(1)) + ptab(3)*(cum(2)-ptab(2))*(1-cum(1)) + ptab(3)*(cum(1)-ptab(1))*(1-cum(2)) ) *cum(4)+(ptab(3)*(cum(2)-ptab(2))*ptab(1) + ptab(3)*(cum(1)-ptab(1))* ptab(2) +ptab(1)*(cum(3)-ptab(3))* ptab(2) )+ptab(1)*ptab(2)*ptab(3) +(ptab(1)*ptab(2)*(1-cum(3)) +   ptab(1)*ptab(3)*(1-cum(2)) + ptab(3)*ptab(2)*(1-cum(1))) *  cum(4)    ;// 3 faulty and  2 min;
		if (val> 0) then
			dx=dtab(1);
			xy.state(dx)=dx;
			xy.probability(dx)=val;
		end;			
		if (mini==1) then pcum = pcum - p1; qcum  = qcum - p2;  rcum = rcum - p3;  i = i+1 ;  j = j+1 ;  k = k+1 ;  end;
		if (mini==2) then qcum = qcum - p2; rcum  = rcum - p3;  wcum = wcum - p4;  j = j+1 ;  k = k+1 ;  l = l+1 ;  end;
		if (mini==3) then pcum = pcum - p1; wcum  = wcum - p4;  qcum = qcum - p2;  i = i+1 ;  j = j+1 ;  l = l+1 ;  end;
		if (mini==4) then pcum = pcum - p1; rcum  = rcum - p3;  wcum = wcum - p4;  i = i+1 ;  k = k+1 ;  l = l+1 ;  end;
    else // 2 outof 4 when all are equal  
		val=(ptab(1)*(cum(2)-ptab(2))*(cum(3)-ptab(3)) + ptab(2)* (cum(1)-ptab(1))*(cum(3)-ptab(3)) +ptab(3)*(cum(2)-ptab(2))*(cum(1)-ptab(1)))    * (1-cum(4)+ptab(4)) + ((1-cum(1))*(cum(2)-ptab(2))*(cum(3)-ptab(3)) + (1-cum(2))* (cum(1)-ptab(1))*(cum(3)-ptab(3)) +(1-cum(3))*(cum(2)-ptab(2))*(cum(1)-ptab(1)))     * ptab(4)+ (ptab(1)*(cum(2)-ptab(2))*(1-cum(3)) + ptab(1)*(cum(3)-ptab(3))*(1-cum(2)) +ptab(2)*(cum(1)-ptab(1))*(1-cum(3)) + ptab(2)*(cum(3)-ptab(3))*(1-cum(1)) +ptab(3)*(cum(2)-ptab(2))*(1-cum(1)) + ptab(3)*(cum(1)-ptab(1))*(1-cum(2)) ) *cum(4)+ (ptab(3)*(cum(2)-ptab(2))*ptab(1) + ptab(3)*(cum(1)-ptab(1))* ptab(2) +ptab(1)*(cum(3)-ptab(3))* ptab(2) )+ptab(1)*ptab(2)*ptab(3) +(ptab(1)*ptab(2)*(1-cum(3)) +   ptab(1)*ptab(3)*(1-cum(2)) + ptab(3)*ptab(2)*(1-cum(1))) *  cum(4)    ;// 3 faulty and  2 min;						
		if (val> 0) then
			dx=dtab(1);
			xy.state(dx)=dx;
			xy.probability(dx)=val;
		end;
		pcum = pcum - p1; qcum = qcum - p2; rcum = rcum - p3; wcum = wcum - p4;
		i = i+1 ; j = j+1 ; k = k+1 ; l = l+1 ; 	
    end; // end select
end; //end While

xyBis=dFTr_sortState(xy);
xy=xyBis;

xyBis=dFTr_compact(xy);
xy=xyBis;

endfunction



