function [f] = DFT(fdepstring,s)
// internal recursive function,  locate the area where there is a dependance and
// replaces by a tliste after having done the combinatorial computation
// Calling Sequence
//   Y=findDep(liste,args)
// Parameters
//   liste : a list that contains a DFT of string.
//
// Description
//   returns Y a liste that contains a DFT in string without dependance.


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         liste : a list that contains tlist of string
// Returns:
//       returns Y a liste that contains a DFT of string without dependance.
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)
if fdepstring == "" then 
	f=DepSubTree(s);
else
	
	s=replaceFDEP(fdepstring,s);
	f=DepCombinatoire(s);
end;
clearglobal args;
clearglobal argate;
clearglobal listdepen;

endfunction;