function [s] = replaceFDEP(fdepstring,s)
// internal recursive function,  locate the area where there is a dependance and
// replaces by a tliste after having done the combinatorial computation
// Calling Sequence
//   Y=findDep(liste,args)
// Parameters
//   liste : a list that contains a DFT of string.
//
// Description
//   returns Y a liste that contains a DFT in string without dependance.


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         liste : a list that contains tlist of string
// Returns:
//       returns Y a liste that contains a DFT of string without dependance.
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)
gates=list("SPAND","PAND","AND","OR","CSP","HSP","SEQ","1/3","1/4","2/3","2/4","3/3","3/4","4/4","FDEP");
tok = ['(',')',','];
[porte,paren] = strsplit(s,tok);
i = 1;
j = 1;
sizeporte=size(porte);
sizeparen=size(paren);
expList=list();
while (i<sizeporte(1)+1) &  (j <sizeparen(1)+1)
	if length(porte(i)) == 0 then 
		i=i+1;
	else
			b=%F;
			for x=1: length(gates)
				if strcmp(porte(i),gates(x))==0 then 
					b=%T;
				end;
			end;
			if b == %F then 
				expList($+1)=porte(i)+"KK";
			else 
				expList($+1)=porte(i);
			end;
		i = i+1;
	end;
	if length(paren(j)) == 0 then 
		j = j+1;
	else
		expList($+1)=paren(j);
		j=j+1;
	end;
end;


s="";
for i=1 : length(expList) 
	s=s+expList(i);
end;

tok = ["FDEP(",")",","];
[porte,paren] = strsplit(fdepstring,tok);
sizeporte=size(porte);
expList=list();
i=1;
while (i<sizeporte(1)+1)
	if length(porte(i)) == 0 then 
		i=i+1;
	else
		expList($+1)=porte(i);
		i = i+1;
	end;
end;
k=expList(1);
print(%io(2),length(expList))
for i=2 : length(expList)
	print(%io(2),s)
	l="OR("+k+","+expList(i)+")";
	x=expList(i)+"KK";
	s=strsubst(s,x,l);
	print(%io(2),l)
end;
s=strsubst(s,"KK","");

print(%io(2),s)
endfunction;