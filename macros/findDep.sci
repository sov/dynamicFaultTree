function [f] = findDep(liste,marquage)
// internal recursive function,  locate the area where there is a dependance and
// replaces by a tliste after having done the combinatorial computation
// Calling Sequence
//   Y=findDep(liste,args)
// Parameters
//   liste : a list that contains a DFT of string.
//
// Description
//   returns Y a liste that contains a DFT in string without dependance.


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         liste : a list that contains tlist of string
// Returns:
//       returns Y a liste that contains a DFT of string without dependance.
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)
if typeof(liste) ~= typeof(list()) then  
	f=list(string(liste),marquage);
else
	global('argate');
	x="";
	x=argate(1);
	argate(1)=null();
	r="";
	d=list();
	for i=1 : length(liste)
		b=findDep(liste(i),marquage(i));
		if i == length (liste)
			r=r+b(1); 
		else 
			r=r+b(1)+","; 
		end;
		d($+1)=b(2);
	end;
	r=x+"("+r+")";
	print(%io(2),r);
	global('listdepen');
	listdepen=list();
	c=isDependant(r,d);
	if c==%T then
		d=listdepen(1);
		print(%io(2),r);
		w=rand();
		z=strsplit(string(w),".")
		newV="v"+z(2);
		//execstr("global('''+"v"+z(2)+'''+")" ");
               execstr('global('''+"v"+z(2)+''') ');
		//execstr("r='''+"v"+z(2)+'''");
               execstr('r='''+"v"+z(2)+''' ');
	end;
	f=list(r,d);
	print(%io(2),c,f);
end;

endfunction;
