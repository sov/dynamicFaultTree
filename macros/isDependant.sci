function [xy] = isDependant(s,marquage)
// return a boolean if the instruction s containt dependant leaf
// Calling Sequence
//   Z=isDependant(s)
// Parameters
//   s : a instruction in String
//
// Description
//   return a boolean if the instruction s containt dependant's leaf
// Examples
// s="OR(AND(v1,v2),AND(v2,v3))"

// Z=isDependant(s)


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         s: a string
// Returns:
//         xy: boolean isDependant(s) 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)
xy=%F;
gates=list("SPAND","PAND","AND","OR","CSP","HSP","SEQ","1/3","1/4","2/3","2/4","3/3","3/4","4/4","FDEP");
tok = ['(',')',','];
v= tokens(s,tok);
sz=size(v);
sz=sz(1);
vecteur = list();
global('listdepen');
//Extraction des arguments et ajout dans la liste vecteur
for i=1 : sz
	b=%F;
	for x=1: length(gates)
		if strcmp(v(i),gates(x))==0 then 
			b=%T;
		end;
	end;
	if b == %F then 
		vecteur(0)=v(i);
	end;
end;

//Test si doublon donc dependant sinon non dependant
for i=1 : length(vecteur)
	for j = 1 : length(vecteur)
		if j~=i & vecteur(j)==vecteur(i) then 
			xy=%T;
			listdepen($+1)=vecteur(j);
		end;
	end;
end;

for i=1 : length(vecteur)
	temp="list("+applatieTree(marquage)+");"
	execstr("marquage="+temp);
	global('args');
	marquage=replace(marquage,args);
	for j = 1 : length(marquage)
		if marquage(j)==vecteur(i) then 
			xy=%T;
			listdepen($+1)=marquage(j);
		end;
		print(%io(2),marquage(j),vecteur(i))
	end;
end;
print(%io(2),xy);
endfunction
