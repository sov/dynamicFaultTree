function [xy] = dFTr_merge(x,y)
//  distribution tlist  merge
// Calling Sequence
//   Z=dFTr_merge(X,Y)
// Parameters
//   X : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//   Y : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//   Z : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//
// Description
//   returns <varname>Z</varname> the sorted distribution tlist merge of <varname>X</varname> and <varname>Y</varname> defined by :
// <para>
//    <latex>
// \Pr(Z =a)=  \Pr(X =a) + \Pr(Y =a).
//    </latex>
//    </para>
//  <para>
//  The sum of the cumulative sum of <varname>X.probability</varname> and    
//  the cumulative sum of <varname>Y.probability</varname> must be smaller than 1.
//  </para>
// Examples
// X=tlist(['distribution','state','probability'],[2,9,15],0.3*[0.25,0.5,0.25])
// Y=tlist(['distribution','state','probability'],[5,7,9],0.7*[0.3,0.1,0.6])
// Z=dFTr_merge(X,Y)
// //  Expected : Z.state       2.    5.   7.    9.   15.
// //             Z.probability 0.075 0.21 0.07  0.57  0.075

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);

//%
// Output variables initialisation (not found in input variables)
xyLength=length(x.state)+length(y.state);
xy=tlist(['distribution','state','probability'],zeros(1,xyLength),zeros(1,xyLength));
k=0;
p=1;
q=1;

while (p <= length(x.state)) & (q <= length(y.state))
  k=k+1;
  if x.state(p) < y.state(q) then
    xy.state(k)=x.state(p);
    xy.probability(k)=x.probability(p); 
    p=p+1;
  elseif  x.state(p) > y.state(q) then
    xy.state(k)=y.state(q);
    xy.probability(k)=y.probability(q);
    q=q+1;
  else
    xy.state(k)=x.state(p);
    xy.probability(k)=x.probability(p)+y.probability(q);
    p=p+1;
    q=q+1;
  end;
end;

while (p <= length(x.state)) 
  k=k+1;
  xy.state(k)=x.state(p);
  xy.probability(k)=x.probability(p);
  p=p+1;
end;
while  (q <= length(y.state))
  k=k+1;
  xy.state(k)=y.state(q);
  xy.probability(k)=y.probability(q);
  q=q+1;
end;

xy.state=resize_matrix(xy.state,1,k);
xy.probability=resize_matrix(xy.probability,1,k);

cumXY=sum(xy.probability);
if cumXY<1 then
  warning('dFTr_merge cum sum <1');
end;
endfunction

