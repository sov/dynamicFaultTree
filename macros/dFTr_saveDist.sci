function dFTr_saveDist(dist,file1)
// saves a  tlist distribution in a text file.
//
// Calling Sequence
// dFTr_saveDist(dist,file)
// Parameters
//         dist: <link  linkend="attackTree_distribution_tlist">a tlist distribution</link>
//         file:  filename
//
// Description
// saves a tlist distribution in a text file.
// The first line of the file is a comment line which contains the number of states. 
// Otherwise each line contains a  state and the probability of the state.
// A distribution saved in a file can be loaded with the function
// <link  linkend="dFTr_loadDist">dFTr_loadDist</link>.
//
// Examples
// X=tlist(['distribution','state','probability'],[2,9,15],[0.25,0.5,0.25])
// dFTr_saveDist(X,'X')
// // The file 'X' contains
// // # 3
// // 2.000000000000000e+00 2.500000000000000e-01 
// // 9.000000000000000e+00 5.000000000000000e-01 
// // 1.500000000000000e+01 2.500000000000000e-01 



//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);

[m,n]=size(dist.state);
comment=msprintf('# %d',n);
//print(%io(2),comment);
//print(%io(2),z);
mat=cat(2,dist.state' ,dist.probability');
fprintfMat(file1, mat,'%.15e',comment );
endfunction



