function [xy] = dFTr_3of3(v1,v2,v3)
// sorted distribution tlist 3 out of 3 (3/3)
// Calling Sequence
//   Z=dFTr_3of3And(v1,v2,v3)
// Parameters
//   v1 : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   v2 : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   v3 : <link  linkend="dFTr_distribution_tlist">a sorted tlist distribution tlist.</link>
//
// Description
//   Returns Z the sorted distribution tlist 3 out of 3 of v1,v2 and v3 defined by :
// <para>
//<latex> \LARGE
//$ min \{ v_i | \sum_{j=1}^I {\mathbb{1}_{(v_j<v_i)} } \geq k \} $ 
//    </latex>
// </para>
// Examples
// v1=tlist(['distribution','state','probability'],[2,4,10],[0.2,0.4,0.4])
// v2=tlist(['distribution','state','probability'],[1,3,4,10],[0.1,0.3,0.2,0.4])
// v3=tlist(['distribution','state','probability'],[2,3,5,10],[0.2,0.3,0.2,0.5])

// Z=dFTr_3of3(v1,v2,v3)


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         v1: tlist distribution
//         v2: tlist distribution
//         v3: tlist distribution
// Returns:
//       tlist distribution dFTr_3of3(v1,v2,v3) 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)

xyLength=length(v1.state)+length(v2.state)+length(v3.state);
xy=tlist(['distribution','state','probability'],zeros(1,xyLength),zeros(1,xyLength));

xy=dFTr_and(v1,v2);
xy=dFTr_and(xy,v3);

endfunction

