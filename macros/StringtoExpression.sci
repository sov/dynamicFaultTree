function [xy] = StringtoExpression(s)
// return the execution result by replacing the equivalent function name
// Calling Sequence
//   Z=StringtoExpression(s)
// Parameters
//   s : a instruction in String

// Description
//   Execute Scilab code s in strings  ;
// <para>
// </para>
// Examples
// v1=tlist(['distribution','state','probability'],[2,4,10],[0.2,0.4,0.4])
// v2=tlist(['distribution','state','probability'],[1,3,4,10],[0.1,0.3,0.2,0.4])
// v3=tlist(['distribution','state','probability'],[2,3,5,10],[0.2,0.3,0.2,0.5])
// s ="OR(AND(v1,v2),AND(v2,v3))"

// Z=StringtoExpression(s)


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         s: a string
// Returns:
//       result 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)
xy=%F;
gates=list(["SPAND","attackTree_spand"],["PAND","attackTree_pand"],["AND","attackTree_and"],["OR","attackTree_or"],["CSP","attackTree_CSP"],["HSP","attackTree_HSP"],["SEQ","attackTree_seq"],["1/3","attackTree_1of3Or"],["1/4","attackTree_1of4Or"],["2/3","attackTree_2of3"],["2/4","attackTree_2of4"],["3/3","attackTree_3of3"],["3/4","attackTree_3of4"],["4/4","attackTree_4of4"]);
//replace all gates by equivalent name of function
for i=1 : length(gates)
	s = strsubst(s, gates(i)(1), gates(i)(2));
end;
s="xy="+s;
execstr(s);
endfunction