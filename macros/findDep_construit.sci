function [f] = findDep_construit(liste)
// internal recursive function,  locate the area where there is a dependance and
// replaces by a tliste after having done the combinatorial computation
// Calling Sequence
//   Y=findDep(liste,args)
// Parameters
//   liste : a list that contains a DFT of string.
//
// Description
//   returns Y a liste that contains a DFT in string without dependance.


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         liste : a list that contains tlist of string
// Returns:
//       returns Y a liste that contains a DFT of string without dependance.
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)
if typeof(liste) ~= typeof(list()) then  
	f=string(liste);
else
	global('argate');
	x="";
	x=argate(1);
	argate(1)=null();
	r="";
	for i=1 : length(liste)

		b=findDep_construit(liste(i));
		if i == length (liste)
			r=r+b; 
		else 
			r=r+b+",";
		end;
	end;
	r=x+"("+r+")";
	f=r;
end;
endfunction;