function [xy] = dFTr_and(x,y)
//  sorted distribution tlist conjunction (and)
// Calling Sequence
//   Z=dFTr_and(X,Y)
// Parameters
//   X : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//   Y : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//   Z : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//
// Description
//   returns <varname>Z</varname> the sorted distribution tlist conjunction of <varname>X</varname> and <varname>Y</varname> defined by :
// <para>
//    <latex>
// \Pr(Z =a)=  \Pr(X =a) \times \Pr(Y <a)+ \Pr(Y =a) \times \Pr(X<a)+ \Pr(X =a) \times \Pr(Y =a).
//    </latex>
//    </para>
// Examples
// X=tlist(['distribution','state','probability'],[2,9,15],[0.25,0.5,0.25])
// Y=tlist(['distribution','state','probability'],[5,7],[0.3,0.7])
// Z=dFTr_and(X,Y)
// //  Expected : Z.state       5.   7.     9.  15.
// //             Z.probability 0.075 0.175 0.5 0.25

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);

//%
// Output variables initialisation (not found in input variables)
xyLength=length(x.state)+length(y.state);
xy=tlist(['distribution','state','probability'],zeros(1,xyLength),zeros(1,xyLength));
k=xyLength+1;
p=length(x.state);
q=length(y.state);
XCum=1;
YCum=1;
while (p>=1) & (q >=1)
  k=k-1;
  if x.state(p) > y.state(q) then
    xy.state(k)=x.state(p);
    xy.probability(k)=x.probability(p)*YCum;
    XCum=XCum-x.probability(p);
    p=p-1;
    if xy.probability(k)<0 then
      error('negative probability');
    end;
  elseif x.state(p) < y.state(q) then
    xy.state(k)=y.state(q);
    xy.probability(k)=y.probability(q)*XCum;
    YCum=YCum-y.probability(q);
    q=q-1;
    if xy.probability(k)<0 then
      error('negative probability');
    end; 
  else
    xy.state(k)=x.state(p);
    xy.probability(k)=abs(x.probability(p)*YCum+y.probability(q)*XCum-x.probability(p)*y.probability(q));
    XCum=XCum-x.probability(p);
    YCum=YCum-y.probability(q);
    p=p-1;
    q=q-1;
    if xy.probability(k)<0 then
      error('negative probability');
    end;
  end;
end;

xy.state=xy.state(k:xyLength);
xy.probability=xy.probability(k:xyLength);
cumXY=sum(xy.probability);
if cumXY<1 then
  warning('dFTr_and cum sum <1');
end;

endfunction
