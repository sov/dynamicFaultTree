function [y] = dFTr_oneMin(dist1,dist2,dist3)
// internal function, return a vector : Probaility , Indentifier , State  to identify wich one of input is the less.
// Calling Sequence
//   Y=dFTr_oneMin(dist1,dist2,dist3)
// Parameters
//   dist1 : a state
//   dist2 : a state 
// 	 dist3 : a state
//
// Description
// return a vector : Probaility , Indentifier , State  to identify wich one of input is the less.

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);
y=list();
if ((dist1<dist2) &  (dist1<dist3)) then
	val= p1*(1-qcum)*rcum + p1*qcum*(1-rcum);
	y($+1)=val;
	y($+1)=1;
	y($+1)=dist1;
else
	if ((dist2<dist1) & (dist2<dist3)) then
		val= p2*(1-pcum)*rcum + p2*pcum*(1-rcum);
		y($+1)=val;
		y($+1)=2;
		y($+1)=dist2;
	else
		if ((dist3<dist2) & (dist3<dist1)) then
			val= p3*(1-pcum)*qcum + p3*pcum*(1-qcum);
			y($+1)=val;
			y($+1)=3;
			y($+1)=dist3;
		end;
	end;
end;
endfunction;
