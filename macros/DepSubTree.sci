function [xy] = DepSubTree(s)
// function that finds the smallest subtree where there is a dependency
// Calling Sequence
//   Y=DepSubTree(s)
// Parameters
//   s : a instruction in String representing a DFT.
// Description
//   Returns Y the sorted distribution tlist 
// Examples
// v1=tlist(['distribution','state','probability'),[2,4,10),[0.2,0.4,0.4))
// v2=tlist(['distribution','state','probability'),[1,3,4,10),[0.1,0.3,0.2,0.4))
// v3=tlist(['distribution','state','probability'),[2,3,5,10),[0.2,0.2,0.1,0.5))
// v4=tlist(['distribution','state','probability'),[2,4,10),[0.2,0.4,0.4))
// s="OR(AND(v1,v2),AND(v3,HSP(v4,v3)))"

// Z=DepSubTree(s)


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//   s : a instruction in String representing a DFT.
// Returns:
//   Returns Y the sorted distribution tlist :

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)

gates=list("SPAND","PAND","AND","OR","CSP","HSP","SEQ","1/3","1/4","2/3","2/4","3/3","3/4","4/4","FDEP");
tok = ["(",")",","];
[porte,paren] = strsplit(s,tok);
i = 1;
j = 1;
//FUSSION DES DEUX LISTE parsé avec ["(",")",","];
sizeporte=size(porte);
sizeparen=size(paren);
expList=list();
while (i<sizeporte(1)+1) &  (j <sizeparen(1)+1)
	if length(porte(i)) == 0 then 
		i=i+1;
	else
		expList($+1)=porte(i);
		i = i+1;
	end;
	if length(paren(j)) == 0 then 
		j = j+1;
	else
		expList($+1)=paren(j);
		j = j+1;
	end;
end;
//Creation de la liste de liste avec les portes
j=0;
s="";
global('argate');
argate=list();
args=list();
for i=1 : length(expList)
	b=%F;
	for x=1 : length(gates)
		if expList(i) == gates(x) then
			b=%T;
			break;
		end; 
	end;
	if b == %T then
		argate($+1)=expList(i);
	else
		if expList(i) == "("  then
			expList(i)="list(";
			expList(i-1)="";
		else 
			if expList(i) ~= "," & expList(i) ~= ")" then 
				args($+1)=expList(i);
			end;
		end;
	end;
end;

for i=1 : length(expList)
	s=s+expList(i);
end;

argatebis=argate;

print(%io(2),s);
execstr("MAMA="+s);
global('args');

//remplace l'equivalent de la variable en string args contient les string
MAMA=replace(MAMA,args);

typ=tlist(['distribution','state','probability'],[],[]);
marquage=replaceWithNull(MAMA);
global('listdepen');

n=getdate("s");
rand("seed",n);
a=findDep(MAMA,marquage);
f=a(1);


print(%io(2),"88888888888888888888888888888888888888888888888888888_")
argate=argatebis;
popo=findDep_computation(MAMA,a(2));

print(%io(2),popo(1));



v=tokens(popo(1),tok);
sz=size(v);
sz=sz(1);
vecteur = list();
//declaration en tant que gloabl
for i=1 : sz
	b=%F;
	for x=1: length(gates)
		if strcmp(v(i),gates(x))==0 then 
			b=%T;
		end;
	end;
	if b == %F then 
		if exists(v(i))==0 then
			global(v(i));
		end;
	end;
end;
xy=StringtoExpression(popo(1));

endfunction;