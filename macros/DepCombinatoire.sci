function [rFinal] = DepCombinatoire(s)
// internal function,  When there is a dependency, the computational function with a vFix, 
// combinational computation
// Calling Sequence
//   Y=DepCombinatoire(s)
// Parameters
//   s : a instruction in String representing a DFT.
// Description
//   returns <link  linkend="attackTree_distribution_tlist">a sorted tlist distribution tlist.</link>


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//   s : a instruction in String representing a DFT.
// Returns:
//   returns <link  linkend="attackTree_distribution_tlist">a sorted tlist distribution tlist.</link>
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)
xy=%F;
gates=list("SPAND","PAND","AND","OR","CSP","HSP","SEQ","1/3","1/4","2/3","2/4","3/3","3/4","4/4","FDEP");
tok = ['(',')',','];
v= tokens(s,tok);
sz=size(v);
sz=sz(1);
vecteur = list();
//Extraction des arguments et ajout dans la liste vecteur
for i=1 : sz
	b=%F;
	for x=1: length(gates)
		if strcmp(v(i),gates(x))==0 then 
			b=%T;
		end;
	end;
	if b == %F then 
		c=strcat(v(i)," ");
		vecteur($+1)=c;
		if exists(v(i))==0 then
			global(v(i));
		end;
	end;
end;

vdep=list();
//Test si doublon et ajoute dans une liste les données
for i=1 : length(vecteur)
	for j = 1 : length(vecteur)
		if j~=i & vecteur(j)==vecteur(i) then 
			xy=%T;
			vdep($+1)=vecteur(i);
		end;
	end;
end;

rFinal=tlist(['distribution','state','probability'],[],[]);

for i = 1 : length(vdep)
	execstr("vns="+vdep(i));
	m="vtest("+string(i)+")";
	a=strcat(vdep(i)," ");
	s = strsubst(s,a,m);
	for x =1 : length(vns.state)
		vtest=list();
		vtest(i)=tlist(['distribution','state','probability'],[vns.state(x)],[1]);
		//Execution avec un VFIXE
		//print(%io(2),"XXXXXXXXXXXX");
		//execstr("print(%io(2),vtest(i));");
		rfixe = StringtoExpression(s);
		//print(%io(2),rfixe,s);
		//print(%io(2),"XXXXXXXXXXXX");

		//Fois 
		for c =1 : length (rfixe.state)
			rfixe.probability(c)=rfixe.probability(c)*vns.probability(x);
		end;
		//Ajout a RFinal+Rfinal pour ensuite 
		for c =1 : length (rfixe.state)
			rFinal.state($+1)=rfixe.state(c);
			rFinal.probability($+1)=rfixe.probability(c);
		end;
		//Redimensionnement
		rFinal.state=matrix(rFinal.state,[1,length(rFinal.state)]);
		rFinal.probability=matrix(rFinal.probability,[1,length(rFinal.probability)]);
	end;
	rFinal=attackTree_sortState(rFinal);
	rFinal=attackTree_compact(rFinal);
	break;
end;
endfunction;