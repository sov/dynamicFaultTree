function [xy] = dFTr_seq(x,y)
// sorted  distribution tlist sequential conjunction (seq)
// Calling Sequence
//   Z=dFTr_seq(X,Y)
// Parameters
//   X : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//   Y : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//   Z : <link  linkend="attackTree_distribution_tlist">a sorted distribution tlist</link>
//
// Description
//   Returns <varname>Z</varname> the sorted distribution tlist sequential  conjunction of <varname>X</varname> and <varname>Y</varname> defined by : 
// <para>
//    <latex>
//      \Pr (Z=a)= \sum_{k}  \Pr (X=k) \times \Pr(Y= a-k).
//    </latex>
//    </para>
// Examples
// X=tlist(['distribution','state','probability'],[2,9,15],[0.25,0.5,0.25])
// Y=tlist(['distribution','state','probability'],[5,7],[0.3,0.7])
// Z=dFTr_seq(X,Y)
// // Expected : Z.state        7.    9.    14.  16.  20.   22.
// //            Z.probability  0.075 0.175 0.15 0.35 0.075 0.175

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);


// Output variables initialisation (not found in input variables)
nx=length(x.state);
ny=length(y.state);
xyLength=nx*ny;
xy=tlist(['distribution','state','probability'],zeros(1,xyLength),zeros(1,xyLength));
[k,xy.state,xy.probability]= call("atkTr_seq_c",nx,4,"d",x.state,5,"d",x.probability,6,"d",ny,7,"d",y.state,8,"d",y.probability,9,"d","out",[1,1],1,"d",[1,xyLength],2,"d",[1,xyLength],3,"d");


xy.state=resize_matrix(xy.state,1,k);
xy.probability=resize_matrix(xy.probability,1,k);
cumXY=sum(xy.probability);
if cumXY<1 then
  warning('dFTr_seq cum sum <1 ');
end;


endfunction
