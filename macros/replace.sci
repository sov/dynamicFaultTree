function [f] = replace(liste,args)
// internal recursive function,  replace variable by the same variable in string 
// Calling Sequence
//   Y=replace(liste,args)
// Parameters
//   liste : a list that contains a DFT of tlist.
//   args : a list that contains the tlist names in string
//
// Description
//   returns Y a liste that contains a DFT in string.


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         liste : a list that contains tlist.
//   		args : a A list that contains the tlist names in string.

// Returns:
//       returns Y a liste that contains a DFT of string.
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)
if typeof(liste) ~= typeof(list()) then  
	if typeof(liste) == typeof(0) then
	else
		for x=1 : length(args)
				execstr("dxdx="+args(x));
				if liste == dxdx then
					liste=args(x);
					break;
				end;
		end;
	end;
	f=liste;
else
		k=list();
		for i=1 : length(liste)
			k($+1)=replace(liste(i),args);
		end;
		f=k;
end;
endfunction;