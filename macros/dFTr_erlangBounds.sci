function [dist_inf,dist_sup] =dFTr_erlangBounds(shape1,rate1,time,size1)
// discrete Erlang distribution bounds 
// Calling Sequence
// [dist_inf,dist_sup] =dFTr_erlangBounds(shape,rate,time,size)
// Parameters
//         shape: shape parameter of the Erlang distribution 
//         rate: rate parameter of the Erlang distribution
//          time: end of simulation time
//          size: inverse of the discretization interval width
//
// Description
//      returns  a discrete lower bounding 
// <link  linkend="attackTree_distribution_tlist"> sorted distribution tlist</link>
// <varname>dist_inf</varname>
// and a discrete upper bounding  sorted distribution tlist <varname>dist_sup</varname>  
// of an Erlang distribution with parameters  <varname>shape</varname> and <varname>rate</varname>. The product <varname>time</varname>*<varname>size</varname> is the number of bins of the bounding distributions.
//
// Examples
// // Boundings with 30 bins
// [dist_inf30,dist_sup30] =dFTr_erlangBounds(2,2,5,6)
// // Boundings with 100 bins
// [dist_inf100,dist_sup100] =dFTr_erlangBounds(2,2,5,20)
// // Continous cumulative distribution function
// P=cdfgam("PQ",dist_inf30.state,2*ones(1,30),2*ones(1,30))
// // Plots of the  cumulative distribution functions
// clf()
// plot2d2(dist_inf30.state,cumsum(dist_inf30.probability),color('orange'))
// plot2d2(dist_sup30.state,cumsum(dist_sup30.probability),color('purple'))
// plot2d2(dist_inf100.state,cumsum(dist_inf100.probability),color('red'))
// plot2d2(dist_sup100.state,cumsum(dist_sup100.probability),color('green'))
// plot2d(dist_inf30.state,P,color('blue'))
// legends(['inf30';'inf100';'continuous';'sup100';'sup30'],..
//[color('orange') color('red') color('blue') color('green') color('purple')],..
//opt="lr")

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);


// Output variables initialisation (not found in input variables)
vecLength=time*size1+1;
v=0:1/size1:time;
z=0:1/size1:time;
//print(%io(2),v);
//print(%io(2),vecLength);
for i=2:vecLength
  z(i)=cdfgam('PQ',v(i),shape1,rate1)-cdfgam('PQ',v(i-1),shape1,rate1);
  if z(i) < 0 then
    error('negative probability');
  end
  if z(i) <= %eps then
     print(%io(2),'proba too small',i,z(i));
     warning('dFTr_erlangBounds : probability too small');
  end;
    
end
comment=msprintf('# %d',vecLength-1);
//print(%io(2),comment);
//print(%io(2),z);
remain=0;
if sum(z) < 1 then
 remain=1-sum(z);
 end
z_inf=z;
z_sup=z;
// suppress the first element  of z
z(:,1)=[];
z_inf=z;
z_inf(1)=z_inf(1)+remain;
z_sup=z;
v_inf=v;
v_inf(:,vecLength)=[];
v_sup=v;
v_sup(:,1)=[];
//if remain>0 then
if remain>%eps then
 z_sup(vecLength-1)=z_sup(vecLength-1)+remain;
end

dist_inf=tlist(['distribution','state','probability'],v_inf,z_inf);
dist_sup=tlist(['distribution','state','probability'],v_sup,z_sup)
//d_inf=cat(2,v_inf' , z');
//d_sup=cat(2,v_sup' , z');
//fprintfMat(fileInf, d_inf,'%.15f',comment );
//fprintfMat(fileSup, d_sup,'%.15f',comment );
endfunction



