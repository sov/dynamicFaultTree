function [y] = dFTr_sortState(x)
// sorts a multiset distribution tlist with the states in ascending order
// Calling Sequence
//   Y=dFTr_sortState(X)
// Parameters
//   X : <link  linkend="attackTree_distribution_tlist">a multiset distribution tlist.</link>
//   Y : <link  linkend="attackTree_distribution_tlist">a multiset distribution tlist.</link>
//
// Description
//   returns Y the multiset distribution tlist obtained from X where
//   the states are sorted in ascending order. 
// Examples
// X=tlist(['distribution','state','probability'],[2,15,9,2],[0.125,0.25,0.5,0.125])
// Y=dFTr_sortState(X)



// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         x: tlist distribution

// Returns:
//       tlist distribution sortStateDistribution(x) 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)

[m,n]=size(x.state);
[m1,n1]=size(x.probability);
//print(%io(2),x.probability);
y=tlist(['distribution','state','probability'],[],[]);
[sortedState,sortedProbability]= call("heapSortSupport",n,1,"d",x.state,2,"d",x.probability,3,"d","out",[m,n],2,"d",[m1,n1],3,"d");

//[sortedState,indexes]=gsort(x.state,'g','i');
//print(%io(2),indexes);
//for i=1:length(indexes)
//  sortedProbability(i)=x.probability(indexes(i));
//end;

y.state=sortedState;
y.probability=sortedProbability;

//print(%io(2),y.probability);

endfunction
