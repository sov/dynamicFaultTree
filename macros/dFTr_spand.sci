function [xy] = dFTr_spand(v1,v2)
//  sorted distribution tlist Strict PAND gate (SPAND)
// Calling Sequence
//   Z=dFTr_spand(X,Y)
// Parameters
//   v1 : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   v2 : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//
// Description
//   returns X the sorted distribution tlist S PAND of X and Y defined by :
// 	The output of this gate become True
//	when all of its inputs have failed in same Time. When the sequence of failures is not respected, the output
//	of the gate is False.
// <para>
//    <latex> \LARGE
// 	\[\mathbb{1}_{(V_1 < ... < V_I)}V_2+\mathbb{1}_{\neg(V_1 < ... < V_I)}EoT\]
//    </latex>
//    </para>
// Examples
// X=tlist(['distribution','state','probability'],[0,3,5,9,11],[0.25,0.1,0.35,0.15,0.15])
// Y=tlist(['distribution','state','probability'],[3,4,5,9,14,16],[0.2,0.2,0.1,0.1,0.1,0.3])
//
// Z=dFTr_spand(X,Y)

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);

//%
// Output variables initialisation (not found in input variables)
i=1;
j=1;
xyLength=length(v1.state)+length(v2.state);
xy=tlist(['distribution','state','probability'],zeros(1,xyLength),zeros(1,xyLength));

pcum=1;
qcum=1;
somme=0;
while (j < length(v2.state)+1)
	p1=v1.probability(i);
 	d1=v1.state(i);
  
  	p2=v2.probability(j);
  	d2=v2.state(j);

  	if d1<d2  then 
		pcum=pcum-p1;
		i=i+1;
  	else 
  		val= (1-pcum)*p2;
		xy.state(d2)= d2;
		xy.probability(d2)= val;
		somme =somme +val;
		j=j+1;
  	end;
end;
xy.state(finaltime+1)= finaltime+1;
xy.probability(finaltime+1)= 1-somme;

xy=dFTr_sortState(xy);
xy=dFTr_compact(xy);



endfunction
