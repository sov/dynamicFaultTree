function [f] = heightTree(liste)
// internal recursive function, Calculation of the height of a tree
// Calling Sequence
//   Y=heightTree(liste)
// Parameters
//   liste : a list that contains a DFT of string.

//
// Description
//   returns Y a int the height

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         liste : a list that contains a DFT of string.

// Returns:
//   returns Y a int the height
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);

if typeof(liste) == typeof(list()) & length(liste) == 0 then 
	f =0;
else
	if typeof(liste) ~= typeof(list()) then  
		f=0;
	else
		r =0;
		for i=1 : length(liste)
			r =max(r,heightTree(liste(i)));
		end;
		f=1+r;
	end;
end;
endfunction;