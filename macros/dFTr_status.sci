function [y] = dFTr_status(dist1,dist2,dist3)
// internal function,  sorts a multiset distribution tlist with the states in ascending order
// Calling Sequence
//   Y=dFTr_sortState(X)
// Parameters
//   s1 : a state
//   s2 : a state 
// 	 s3 : a state
//
// Description
//   returns Y a storted list
//   the states are sorted in ascending order. 

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);

if ((dist1==dist2) & (dist1==dist3)) 	y = 0; end;
if ((dist1==dist2) & (dist3>dist2)) | ((dist1==dist3) & (dist2>dist3)) | ((dist2==dist3) & (dist1>dist3)) 	y = 1; end;
if ((dist1<dist2) & (dist1<dist3)  ) | ((dist2<dist3) & (dist2<dist1)) 	| ((dist3<dist2) & (dist3<dist1)) 	y = 2; end;

endfunction
