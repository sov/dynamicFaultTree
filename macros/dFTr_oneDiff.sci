function [y] = dFTr_oneDiff(dist1,dist2,dist3)
// internal function, return a vector : Probaility , Indentifier , State
// Calling Sequence
//   Y=dFTr_oneDiff(dist1,dist2,dist3)
// Parameters
//   dist1 : a state
//   dist2 : a state 
// 	 dist3 : a state
//
// Description
//   return a vector : Value , Indentifier , State

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);
y=list();
if ((dist1==dist2) &  (dist3>dist2)) then
	val=p1*(1-qcum+p2)*rcum + p1*qcum*(1-rcum) + p2*(1-pcum+p1)*rcum + p2*pcum*(1-rcum) - p1*p2;
	y($+1)=val;
	y($+1)=1;
	y($+1)=dist2;
else
	if ((dist1==dist3) & (dist2>dist3)) then
		val=p1*(1-qcum)*rcum  + p1*qcum*(1-rcum+p3) + p3*(1-pcum+p1)*qcum + p3*pcum*(1-qcum) -p3*p1;
		y($+1)=val;
		y($+1)=2;
		y($+1)=dist3;
	else
		if ((dist2==dist3) & (dist1>dist3)) then
			val=p2*(1-pcum)*rcum +p2*pcum*(1-rcum+p3)+p3*(1-pcum)*qcum +p3*pcum*(1-qcum+p2)-p3*p2 ;
			y($+1)=val;
			y($+1)=3;
			y($+1)=dist3;
		end;
	end;
end;
endfunction;
