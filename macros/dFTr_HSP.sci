function [xy] = dFTr_HSP(x,y)
//  sorted distribution tlist spare gate HSP (HSP)
// Calling Sequence
//   Z=dFTr_HSP(X,Y)
// Parameters
//   X : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   Y : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   Z : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//
// Description
//   returns Z the sorted distribution tlist HSP gate of X and Y defined by :
//	It is used to represent the replacement of a primary component by
//	a spare with the same functionality. Spare components may fail even if they are
//	dormant but the failure time distribution of a dormant component is lower in some
//	sense than the failure time distribution of the component which is operational.
//	A spare component may be ”hot” if the dormant has the same failure time distribution as an operating one
// <para>
//    <latex> \LARGE
// 	\[max\{_{v_1,...,vI}\}\] 
//    </latex>
//    </para>
// Examples
// X=tlist(['distribution','state','probability'],[2,9,15],[0.25,0.5,0.25])
// Y=tlist(['distribution','state','probability'],[5,7],[0.3,0.7])
//
// Z=dFTr_HSP(X,Y)

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);

//%
// Output variables initialisation (not found in input variables)
xy=dFTr_and(x,y);

endfunction
