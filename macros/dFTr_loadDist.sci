function dist=dFTr_loadDist(file1)
// loads a  tlist distribution from a text file.
//
// Calling Sequence
// dist=dFTr_loadDist(file)
// Parameters
//         file:  filename
//
// Description
//  loads <link  linkend="attackTree_distribution_tlist">a tlist distribution </link>  from a text file. 
// The first line of the file is a comment line. Otherwise each line must contain a state and the probability of the state. 
// A distribution saved with the function <link  linkend="dFTr_saveDist">dFTr_saveDist</link> can be loaded.
//
// Examples
// X=tlist(['distribution','state','probability'],[2,9,15],[0.25,0.5,0.25])
// dFTr_saveDist(X,'X') 
// Y=dFTr_loadDist('X')

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);

[mat,comment]=fscanfMat(file1);
// print(%io(2),comment);
// print(%io(2),mat);
tmat=mat';
state=tmat(1,:);
probability=tmat(2,:);
dist=tlist(['distribution','state','probability'],state,probability);
endfunction



