function [f] = replaceWithNull(liste)
// internal recursive function,  replace variable by the same variable in string 
// Calling Sequence
//   Y=replace(liste,args)
// Parameters
//   liste : a list that contains a DFT of tlist.
//   args : a list that contains the tlist names in string
//
// Description
//   returns Y a liste that contains a DFT in string.


// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);


// Arguments:
//         liste : a list that contains tlist.
//   		args : a A list that contains the tlist names in string.

// Returns:
//       returns Y a liste that contains a DFT of string.
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%
// Output variables initialisation (not found in input variables)
r=list();
if typeof(liste) == typeof(list()) & length(liste) == 0 then 
	f=liste;
else
	if typeof(liste) ~= typeof(list()) then  
		for x=1 : length(args)
			liste=0;
		end;
		f=liste;
	else
		k=list();
		for i=1 : length(liste)
			k($+1)=replaceWithNull(liste(i));
		end;
		f=k;
	end;
end;

endfunction;