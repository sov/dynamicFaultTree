function [xy] = dFTr_CSP(x,y)
// sorted  distribution tlist spare gate CSP (CSP)
// Calling Sequence
//   Z=dFTr_CSP(X,Y)
// Parameters
//   X : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   Y : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//   Z : <link  linkend="dFTr_distribution_tlist">a sorted distribution tlist.</link>
//
// Description
//   Returns Z the sorted distribution tlist CSP gate of X and Y defined by :
//It is used to represent the replacement of a primary component by
//a spare with the same functionality. Spare components may fail even if they are
//dormant but the failure time distribution of a dormant component is lower in some
//sense than the failure time distribution of the component which is operational.
//A spare component may be ”cold”, if it cannot fail while it is dormant
// <para>
//    <latex> \LARGE
//      \[min\{_{v_1,...,vI}\}\]
//		</latex>
//    </para>
// Examples
// X=tlist(['distribution','state','probability'],[2,9,15],[0.25,0.5,0.25])
// Y=tlist(['distribution','state','probability'],[5,7],[0.3,0.7])
//
// Z=dFTr_CSP(X,Y)

// Number of arguments in function call
[%nargout,%nargin] = argn(0)

// Display mode
mode(-1);

// Display warning for floating point exception
ieee(1);

xy=dFTr_seq(x,y);

endfunction
