mode(1)
//
// Demo of dFTr_seq.sci
//

X=tlist(['distribution','state','probability'],[2,9,15],[0.25,0.5,0.25])
Y=tlist(['distribution','state','probability'],[5,7],[0.3,0.7])
Z=dFTr_seq(X,Y)
// Expected : Z.state        7.    9.    14.  16.  20.   22.
//            Z.probability  0.075 0.175 0.15 0.35 0.075 0.175
//========= E N D === O F === D E M O =========//
