mode(1)
//
// Demo of dFTr_upperBound.sci
//

[d1_inf50,d1_sup50]=dFTr_erlangBounds(2,1,12.5,4)
d1_upper20=dFTr_upperBound(d1_sup50,20)
clf()
plot2d2(d1_sup50.state,cumsum(d1_sup50.probability),color('red'));
plot2d2(d1_upper20.state,cumsum(d1_upper20.probability),color('green'));
legends(['original';'upper bound'],[color('red') color('green')],opt="lr")
//========= E N D === O F === D E M O =========//
