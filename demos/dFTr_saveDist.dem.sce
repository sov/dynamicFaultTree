mode(1)
//
// Demo of dFTr_saveDist.sci
//

X=tlist(['distribution','state','probability'],[2,9,15],[0.25,0.5,0.25])
dFTr_saveDist(X,'X')
// The file 'X' contains
// # 3
// 2.000000000000000e+00 2.500000000000000e-01
// 9.000000000000000e+00 5.000000000000000e-01
// 1.500000000000000e+01 2.500000000000000e-01
//========= E N D === O F === D E M O =========//
