mode(1)
//
// Demo of dFTr_or.sci
//

X=tlist(['distribution','state','probability'],[2,9,15],[0.25,0.5,0.25])
Y=tlist(['distribution','state','probability'],[5,7],[0.3,0.7])
Z=dFTr_or(X,Y)
// Expected : Z.state         2.   5.    7.
//            Z.probability   0.25 0.225 0.525
//========= E N D === O F === D E M O =========//
