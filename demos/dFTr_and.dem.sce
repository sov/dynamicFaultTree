mode(1)
//
// Demo of dFTr_and.sci
//

X=tlist(['distribution','state','probability'],[2,9,15],[0.25,0.5,0.25])
Y=tlist(['distribution','state','probability'],[5,7],[0.3,0.7])
Z=dFTr_and(X,Y)
//  Expected : Z.state       5.   7.     9.  15.
//             Z.probability 0.075 0.175 0.5 0.25
//========= E N D === O F === D E M O =========//
