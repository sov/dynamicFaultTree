mode(1)
//
// Demo of dFTr_erlangBounds.sci
//

// Boundings with 30 bins
[dist_inf30,dist_sup30] =dFTr_erlangBounds(2,2,5,6)
// Boundings with 100 bins
[dist_inf100,dist_sup100] =dFTr_erlangBounds(2,2,5,20)
// Continous cumulative distribution function
P=cdfgam("PQ",dist_inf30.state,2*ones(1,30),2*ones(1,30))
// Plots of the  cumulative distribution functions
clf()
plot2d2(dist_inf30.state,cumsum(dist_inf30.probability),color('orange'))
plot2d2(dist_sup30.state,cumsum(dist_sup30.probability),color('purple'))
plot2d2(dist_inf100.state,cumsum(dist_inf100.probability),color('red'))
plot2d2(dist_sup100.state,cumsum(dist_sup100.probability),color('green'))
plot2d(dist_inf30.state,P,color('blue'))
legends(['inf30';'inf100';'continuous';'sup100';'sup30'],..
[color('orange') color('red') color('blue') color('green') color('purple')],..
opt="lr")
//========= E N D === O F === D E M O =========//
