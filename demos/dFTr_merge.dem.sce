mode(1)
//
// Demo of dFTr_merge.sci
//

X=tlist(['distribution','state','probability'],[2,9,15],0.3*[0.25,0.5,0.25])
Y=tlist(['distribution','state','probability'],[5,7,9],0.7*[0.3,0.1,0.6])
Z=dFTr_merge(X,Y)
//  Expected : Z.state       2.    5.   7.    9.   15.
//             Z.probability 0.075 0.21 0.07  0.57  0.075
//========= E N D === O F === D E M O =========//
