mode(1)
//
// Demo of dFTr_lowerBound.sci
//

[d1_inf50,d1_sup50]=dFTr_erlangBounds(2,1,12.5,4)
d1_lower20=dFTr_lowerBound(d1_inf50,20)
clf()
plot2d2(d1_inf50.state,cumsum(d1_inf50.probability),color('red'));
plot2d2(d1_lower20.state,cumsum(d1_lower20.probability),color('green'));
legends(['original';'lower bound'],[color('red') color('green')],opt="lr")
//========= E N D === O F === D E M O =========//
