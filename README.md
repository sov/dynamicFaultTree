## Dynamic Fault Tree scilab module


This project is a scilab external module based on macros. 
It was written with scilab 6.0.1.
It contains various functions for quantitative dynamic fault tree analysis 
with stochastic bounds.


To use it, from scilab, in the module directory, execute the following files :

```
exec('builder.sce')
exec('loader.sce')
```

The html documentation is built in the directory `help/en_US/scilab_en_US_help/`.

