#include <math.h>
#define TRUE 1
#define FALSE 0
//----------------------------------------------------------------------------------
void attackTreeInsert_f(int *k, double d_stat[], double d_prob[], double state, double probability){
  int done = FALSE;
  for(int i=0;i<= *k ;i++){
    if(d_stat[i]==state){
      d_prob[i]=d_prob[i]+probability;
      done=TRUE;
      return;
    }
    else if(d_stat[i]>state){
      for(int j=(*k);j>=i;j--){
        d_stat[j+1]=d_stat[j];
        d_prob[j+1]=d_prob[j];
      }
      (*k)=(*k)+1;
      d_stat[i]=state;
      d_prob[i]=probability;
      done=TRUE;
      return;
    }
  }
  if(done==FALSE){
    (*k)=(*k)+1;
    d_stat[*k]=state;
    d_prob[*k]=probability;
    done=TRUE;
  }
}
void attackTreeInsert_l(int *k, double d_stat[], double d_prob[], double state, double probability){
  int done = FALSE;
  if(state>d_stat[*k]){
    (*k)=(*k)+1;
    d_stat[*k]=state;
    d_prob[*k]=probability;
    done=TRUE;
    return;
  }
  for(int i=*k;i>=0 ;i--){
    if(d_stat[i]==state){
      d_prob[i]=d_prob[i]+probability;
      done=TRUE;
      return;
    }
    else if(d_stat[i]<state){
      for(int j=(*k);j>i;j--){
        d_stat[j+1]=d_stat[j];
        d_prob[j+1]=d_prob[j];
      }
      (*k)=(*k)+1;
      d_stat[i+1]=state;
      d_prob[i+1]=probability;
      done=TRUE;
      return;
    }
  }
}

void attackTreeInsert(int *k, double d_stat[], double d_prob[], double state, double probability){
  if(probability < 0)
     exit(-1);
  int done = FALSE;
  if(state>d_stat[*k]){
    (*k)=(*k)+1;
    d_stat[*k]=state;
    d_prob[*k]=probability;
    done=TRUE;
    return;
  }
  int first=0;
  int last=*k;
  int middle=(first+last)/2;
  while (first <= last) {
      if (d_stat[middle] < state)
         first = middle + 1;
      else if (d_stat[middle] == state) {
        d_prob[middle]=d_prob[middle]+probability;
        done=TRUE;
        return;
      }
      else
         last = middle - 1;

      middle = (first + last)/2;
   }
   if (first > last){
      for(int j=(*k);j>last;j--){
        d_stat[j+1]=d_stat[j];
        d_prob[j+1]=d_prob[j];
      }
      (*k)=(*k)+1;
      d_stat[last+1]=state;
      d_prob[last+1]=probability;
      done=TRUE;
      return;
  }
}



void atkTr_seq_c(double *d_n, double d_stat[], double d_prob[], double *x_n, double x_stat[], double x_prob[], double *y_n, double y_stat[], double y_prob[]){
  int k=0;
  int p=0;
  int q=0;
  d_stat[k]=x_stat[p]+y_stat[q];
  d_prob[k]=x_prob[p]*y_prob[q];
  while((p<((*x_n)-1)) && (q<((*y_n)-1))){
    if(x_stat[p+1] < y_stat[q+1]){
      p=p+1;
      for(int i=0;i<=q;i++)
         attackTreeInsert(&k, d_stat, d_prob, x_stat[p]+y_stat[i],x_prob[p]*y_prob[i]);
    }
    else if(x_stat[p+1] > y_stat[q+1]){
      q=q+1;
      for(int i=0;i<=p;i++)
         attackTreeInsert(&k, d_stat, d_prob, x_stat[i]+y_stat[q],x_prob[i]*y_prob[q]);
    }
    else{
      p=p+1;
      for(int i=0;i<=q;i++)
         attackTreeInsert(&k, d_stat, d_prob, x_stat[p]+y_stat[i],x_prob[p]*y_prob[i]);
      q=q+1;
      for(int i=0;i<=p;i++)
         attackTreeInsert(&k, d_stat, d_prob, x_stat[i]+y_stat[q],x_prob[i]*y_prob[q]);
    }
  }
  if(p==((*x_n)-1)){
    while(q<(*y_n)-1){
      q=q+1;
      for(int i=0;i<*x_n;i++)
         attackTreeInsert(&k, d_stat, d_prob, x_stat[i]+y_stat[q],x_prob[i]*y_prob[q]);
      
    }
  }
  if(q==((*y_n)-1)){
    while(p<(*x_n)-1){
      p=p+1;
      for(int i=0;i<*y_n;i++)
         attackTreeInsert(&k, d_stat, d_prob, x_stat[p]+y_stat[i],x_prob[p]*y_prob[i]);
    } 
  }
  *d_n=k+1;
} 
