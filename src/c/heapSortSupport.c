#include <math.h>
//----------------------------------------------------------------------------------
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))
//----------------------------------------------------------------------------------
//Structure
//----------------------------------------------------------------------------------
// must be sorted
// may contain zero probability
//----------------------------------------------------------------------------------
typedef struct distribution
{
	double length;
	double *support;
	double *probability;
}distribution;
//----------------------------------------------------------------------------------
void swap_c(distribution *d, int i, int j)
{
        double tmp;
        tmp=(*d).probability[i];
        (*d).probability[i]=(*d).probability[j];
        (*d).probability[j]=tmp;

        tmp=(*d).support[i];
        (*d).support[i]=(*d).support[j];
        (*d).support[j]=tmp;

}

//----------------------------------------------------------------------------------
void siftDown_c(distribution *d, int start, int end)
{
    int root = start;

    while ( root*2+1 < end ) {
        int child = 2*root + 1;
        if ((child + 1 < end) && ((*d).support[child]<(*d).support[child+1])) {
            child += 1;
        }
        if ((*d).support[root]<(*d).support[child]) {
            swap_c(d,child,root);
            root = child;
        }
	else return;
    }
}

//----------------------------------------------------------------------------------
void heapSortSupport_c(distribution *d)
{
        int start, end;
        int count=(*d).length;
        for (start = (count-2)/2; start >=0; start--) {
        siftDown_c(d, start, count);
    }

    for (end=count-1; end > 0; end--) {
        swap_c(d,end,0);
        siftDown_c(d, 0, end);
    }
}


void heapSortSupport(double *d_n,double d_stat[],double d_prob[]){
    distribution d;
    d.length=*d_n;
    d.support=d_stat;
    d.probability=d_prob;
    heapSortSupport_c(&d);
} 
